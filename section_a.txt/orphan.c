#include<stdio.h>
#include <sys/types.h>
#include <unistd.h>
  
int main()
{
	// Create a child process      
	int pid = fork();
  
	if (pid > 0)
        	printf("Parent process\n");
  
    	else if (pid == 0)
	{
		sleep(20);
		printf("Child process\n"); // Parent process is terminated still child process is running so it becomes zombie process
	}
    return 0;
}
