#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/types.h>


int main()
{
	int i, pid = fork();
	if (pid == 0)
	{
		for (i=0; i<5; i++)
		printf("I am Child\n");
	}
	else
	{
        	printf("I am Parent\n");
        	while (1);
    	}
    	return 0;
}
