#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	char A[] = "ABCD...";
	char a[] = "abcd...";
	FILE *fp;
	fp = fopen("q_5.txt","w");
	if (fp == NULL)
	{
		printf("File does not exist.\n");
		exit (1);
	}
	
	if (fork () == 0)
	{
		fprintf(fp,"%s\n",A);
	}
	else 
	{
		fprintf(fp,"%s\n",a);
	}
	fclose (fp);
	return 0;
}
	
	
