#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main () 
{
	printf("I am parent process.\n");
	fork();
	printf("I am the child process.\n");
	return 0;
}
